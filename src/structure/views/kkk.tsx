import React, {useState} from 'react'
import {
  DragDropContext,
  Draggable,
  DraggingStyle,
  Droppable,
  DropResult,
  NotDraggingStyle,
} from 'react-beautiful-dnd'

interface Item {
  id: string
  color: string
  order: number
  height: string
}

// // fake data generator
// const getItems = (count: number): Item[] =>
//   Array.from({length: count}, (v, k) => k).map((k) => ({
//     id: `item-${k}`,
//     content: `item ${k}`,
//   }))

const test = [
  {
    id: '1',
    color: 'peach',
    order: 1,
    height: '314px',
  },
  {
    id: '2',
    order: '2',
    color: 'lilac',
    height: '254px',
  },
  {
    id: '3',
    color: 'grey',
    order: 3,
    height: '350px',
  },
  {
    id: '4',
    color: 'grey',
    order: 4,
    height: '440px',
  },
]
// a little function to help us with reordering the result
const reorder = (
  list: Item[],
  startIndex: number,
  endIndex: number,
): Item[] => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
const getItemStyle = (
  isDragging: boolean,
  draggableStyle: DraggingStyle | NotDraggingStyle | undefined,
  item: any,
): React.CSSProperties => ({
  // some basic styles to make the items look a bit nicer

  height: item.height,

  // styles we need to apply on draggables
  ...draggableStyle,
})
// const getItemStyle = (
//   isDragging: boolean,
//   draggableStyle: DraggingStyle | NotDraggingStyle | undefined,
// ): React.CSSProperties => ({
//   // some basic styles to make the items look a bit nicer
//   userSelect: 'none',
//   height: item.height,
//
//   // change background colour if dragging
//   background: isDragging ? 'lightgreen' : 'grey',
//
//   // styles we need to apply on draggables
//   ...draggableStyle,
// })

// const QuoteItem = styled.div`
//    width: 200px;
//    border: 1px solid grey;
//    background-color: lightblue;'

// const grid = 8
//
// const getItemStyle = (
//   isDragging: boolean,
//   draggableStyle: DraggingStyle | NotDraggingStyle | undefined,
// ): React.CSSProperties => ({
//   // some basic styles to make the items look a bit nicer
//   userSelect: 'none',
//   padding: grid * 2,

//
//   // change background colour if dragging
//   background: isDragging ? 'lightgreen' : 'grey',
//
//   // styles we need to apply on draggables
//   ...draggableStyle,
// })

// const getListStyle = (isDraggingOver: boolean): React.CSSProperties => ({
//   background: isDraggingOver ? 'lightblue' : 'lightgrey',
//   padding: grid,
//   width: 250,
// })

// eslint-disable-next-line no-undef
const QuoteApp = (preparedCards: any): JSX.Element => {
  const [state, setState] = useState(preparedCards)
  console.log(state)
  const onDragEnd = (result: DropResult): void => {
    // dropped outside the list
    if (!result.destination) {
      return
    }

    // @ts-ignore
    const items: Item[] = reorder(
      // @ts-ignore
      state,
      result.source.index,
      result.destination.index,
    )

    setState(items)
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  // @ts-ignore
  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppable">
        {/* eslint-disable-next-line no-undef */}
        {(provided, snapshot): JSX.Element => (
          <div {...provided.droppableProps} ref={provided.innerRef}>
            {state.map((item: any, index: any) => (
              <Draggable key={item.id} draggableId={item.id} index={index}>
                {/* eslint-disable-next-line no-undef */}
                {(provided, snapshot): JSX.Element => (
                  <div
                    key={item.id}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    style={getItemStyle(
                      snapshot.isDragging,
                      provided.draggableProps.style,
                      item,
                    )}
                    className={`Card ${item.color}-card`}
                    // style={{
                    //   height: item.height,
                    // }}>
                  >
                    <h1>Card {item.order}</h1>
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  )
}
export default QuoteApp
// import React, {useState} from 'react'
// import ReactDOM from 'react-dom'
// import styled from '@emotion/styled'
// import {DragDropContext, Droppable, Draggable} from 'react-beautiful-dnd'
// // @ts-ignore
// import type {Quote as QuoteType} from '../types'
//
// interface Quote {
//   id: any
//   content: any
// }
//
// const initial = Array.from({length: 10}, (v, k) => k).map((k) => {
//   const custom: Quote = {
//     id: `id-${k}`,
//     content: `Quote ${k}`,
//   }
//
//   return custom
// })
//
// const grid = 8
// const reorder = (list: any, startIndex: any, endIndex: any) => {
//   const result = Array.from(list)
//   const [removed] = result.splice(startIndex, 1)
//   result.splice(endIndex, 0, removed)
//
//   return result
// }
//
// const QuoteItem = styled.div`
//   width: 200px;
//   border: 1px solid grey;
//   margin-bottom: ${grid}px;
//   background-color: lightblue;
//   padding: ${grid}px;
// `
//
// function Quote({quote, index}: any) {
//   return (
//     <Draggable draggableId={quote.id} index={index}>
//       {(provided) => (
//         <QuoteItem
//           ref={provided.innerRef}
//           // eslint-disable-next-line react/jsx-props-no-spreading
//           {...provided.draggableProps}
//           // eslint-disable-next-line react/jsx-props-no-spreading
//           {...provided.dragHandleProps}>
//           {quote.content}
//         </QuoteItem>
//       )}
//     </Draggable>
//   )
// }
//
// // eslint-disable-next-line react/prop-types
// const QuoteList = React.memo(function QuoteList({quotes}: any) {
//   // eslint-disable-next-line react/prop-types
//   return quotes.map((quote: QuoteType, index: number) => (
//     <Quote quote={quote} index={index} key={quote.id} />
//   ))
// })
//
// function QuoteApp() {
//   const [state, setState] = useState({quotes: initial})
//
//   function onDragEnd(result: {
//     destination: {index: any}
//     source: {index: any}
//   }) {
//     if (!result.destination) {
//       return
//     }
//
//     if (result.destination.index === result.source.index) {
//       return
//     }
//
//     const quotes = reorder(
//       state.quotes,
//       result.source.index,
//       result.destination.index,
//     )
//
//     // @ts-ignore
//     setState({quotes})
//   }
//
//   return (
//     // @ts-ignore
//     <DragDropContext onDragEnd={onDragEnd}>
//       <Droppable droppableId="list">
//         {(provided) => (
//           // eslint-disable-next-line react/jsx-props-no-spreading
//           <div ref={provided.innerRef} {...provided.droppableProps}>
//             <QuoteList quotes={state.quotes} />
//             {provided.placeholder}
//           </div>
//         )}
//       </Droppable>
//     </DragDropContext>
//   )
// }
//
// export default QuoteApp
